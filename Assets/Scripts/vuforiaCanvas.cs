using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class CanvasBehaviour : MonoBehaviour {

    // Use this for initialization
    // Update is called once per frame
    public Button startButton;
    public Button shirt;
    public Button exitButton;
    public GameObject shirtModel;
    public InputField inputFieldURL;
    public Panel mainPanel;
    public Panel shirtPanel;

    void Start() 
    {
        startButton.onClick.AddListener(StartButtonTask);
    }

    void Update()
    {
        
    }

    void StartButtonTask()
    {
        Debug.Log("Pressed the start button");
        startButton.enabled = false;
        mainPanel.enabled = false;
        shirtPanel.enabled = true;
        shirt.onClick.AddListener(delegate { TaskOnClickClothes(shirt); });
    }

    void TaskOnClickClothes(Button bt)
    {
        Debug.Log("Pressed the cloth button");
        if (bt.tag == "shirt")
        {
            shirtModel.SetActive(true);
            callURL(shirtModel);
        }
    }

    void callURL(GameObject modelClothes)
    {
        inputFieldURL.enabled = true;
        Texture2D tex;
        tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
        WWW www = new WWW(inputFieldURL.text.ToString());
        www.LoadImageIntoTexture(tex);
        GetComponent<Renderer>().material.mainTexture = tex;
        Material myNewMaterial = new Material(Shader.Find("Standard"));
        myNewMaterial.SetTexture("_MainTex", tex);
        modelClothes.GetComponent<MeshRenderer>().material = myNewMaterial;
    }
}

